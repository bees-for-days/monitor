# script is designed to be run either in the root directory, or preferably by
# the make command - keep in mind this will only work if you can compile and
# run OpenCV. On the mac m1 for example, I have to compile the program on the
# pi itself
env GOOS=linux GOARCH=arm GOARM=5 go build -o ./builds/monitor

scp ./builds/monitor pi@raspberrypi:/home/pi/monitor
