package config

import (
	"errors"
	"net/http"
)

type Config struct {
	RegistrationSecretKey string `json:"RegistrationSecretKey"`
	ReportEndpoint        string `json:"reportEndpoint"`
}

type ConfigRequest struct {
	*Config
}

func (c *ConfigRequest) Bind(r *http.Request) error {
	if c.Config == nil || c.Config.ReportEndpoint == "" {
		return errors.New("missing required fields")
	}

	return nil
}

type ConfigResponse struct {
	*Config
}

func NewConfigResponse(config *Config) *ConfigResponse {
	return &ConfigResponse{config}
}

func (cr *ConfigResponse) Render(w http.ResponseWriter, r *http.Request) error {
	// mask the secret before showing
	if cr.RegistrationSecretKey != "" {
		cr.RegistrationSecretKey = "xxxxxxxxxx" + cr.RegistrationSecretKey[:len(cr.RegistrationSecretKey)-6]
	}

	return nil
}
