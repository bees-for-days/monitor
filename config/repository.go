package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

// Retrieve the Config from a json file
func Retrieve() (Config, error) {
	configFile, err := os.Open("./config.json")
	if err != nil {
		return Config{}, err
	}
	defer configFile.Close()

	raw, err := ioutil.ReadAll(configFile)
	if err != nil {
		return Config{}, err
	}

	config := Config{}

	return config, json.Unmarshal(raw, &config)
}

func Save(c Config) error {
	raw, err := json.Marshal(c)
	if err != nil {
		return err
	}

	return ioutil.WriteFile("./config.json", raw, 06440)
}
