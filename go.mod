module gitlab.com/bees-for-days/monitor

go 1.16

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.1
	gocv.io/x/gocv v0.28.0
)
