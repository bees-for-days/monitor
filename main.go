package main

import (
	"io"
	"log"
	"net/http"

	"gitlab.com/bees-for-days/monitor/system"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func main() {
	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	// serves as both a health check and a quick way to gain the unique id of the
	// device - used by the app when registering device to a hive
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, system.CPUSerial())
		w.WriteHeader(200)
	})

	// run setup mode on boot
	err := InitSetupMode()
	if err != nil {
		log.Fatal(err)
	}

	// setup routes, mainly for adjusting camera or terminating setup mode
	r.Get("/setup", cameraStreamHandler)
	r.Delete("/setup", terminateSetupMode)

	// config routes, only can read and write
	r.Get("/config", retrieveConfigHandler)
	r.Put("/config", updateConfig)

	// update routes, for managing binary and models
	r.Post("/update/binary", updatedBinary)

	log.Println("Starting server...")
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", r))
}
