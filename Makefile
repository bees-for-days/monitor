build-remote:
	./scripts/build_deploy_remote.sh

copy-source:
	./scripts/copy_source.sh

build-run:
	go build -o monitor && ./monitor

build:
	go build -o monitor
