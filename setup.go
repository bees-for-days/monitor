package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/bees-for-days/monitor/system"
)

// needed for the mpeg stream frames
const boundaryWord = "MJPEGBOUNDARY"
const headerf = "\r\n" +
	"--" + boundaryWord + "\r\n" +
	"Content-Type: image/jpeg\r\n" +
	"Content-Length: %d\r\n" +
	"X-Timestamp: 0.000000\r\n" +
	"\r\n"

func cameraStreamHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "multipart/x-mixed-replace;boundary="+boundaryWord)

	for {
		jpeg := <-cameraChannel

		header := fmt.Sprintf(headerf, len(jpeg))
		frame := make([]byte, (len(jpeg)+len(header))*2)

		copy(frame, header)
		copy(frame[len(header):], jpeg)

		_, err := w.Write(frame)
		if err != nil {
			break
		}
	}
}

func terminateSetupMode(w http.ResponseWriter, r *http.Request) {
	setupChannel <- true
	w.WriteHeader(200)
}

// setupChannel should be sent to when you wish to terminate setup mode
var setupChannel chan bool
var timeoutChannel chan bool
var cameraChannel chan []byte

// InitSetupMode starts a goroutine which will handle keeping hostapd alive so that a user
// can connect to the raspberry pi's wifi - setup will end after ten minutes with no connected
// users or if a user kills it manually by hitting the endpoint
func InitSetupMode() error {
	timeoutChannel = make(chan bool, 1)
	setupChannel = make(chan bool, 1)

	go func(timout chan bool) {
		for {
			if !system.InSetupMode {
				break
			}

			time.Sleep(time.Minute * 5)
			timeoutChannel <- true
		}
	}(timeoutChannel)

	go setupModeLoop()

	err := system.StartSetupMode()
	if err != nil {
		return err
	}

	cameraChannel, err = system.StartCameraStream()

	return err
}

func setupModeLoop() {
	for {
		select {
		case <-setupChannel:
			log.Println("Stopping setup mode...")
			system.StopSetupMode()
			break
		case <-timeoutChannel:
			if system.HasAPClients() {
				log.Println("Device has clients connected, continuing setup mode...")
				continue
			} else {
				log.Println("Stopping setup mode...")
				system.StopSetupMode()
				system.StopCameraStream()
				break
			}
		}
	}
}
