package main

import (
	"net/http"

	"github.com/go-chi/render"
	"gitlab.com/bees-for-days/monitor/config"
)

func retrieveConfigHandler(w http.ResponseWriter, r *http.Request) {
	configFile, err := config.Retrieve()
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return

	}

	if err := render.Render(w, r, config.NewConfigResponse(&configFile)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func updateConfig(w http.ResponseWriter, r *http.Request) {
	newConfig := &config.ConfigRequest{}
	if err := render.Bind(r, newConfig); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	err := config.Save(*newConfig.Config)
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return

	}

	w.WriteHeader(http.StatusOK)
}
