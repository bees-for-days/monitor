package system

import (
	"os/exec"
	"strings"
)

var HardwareID string = ""

// CPUSerial does just what it says, returns the embedded device's serial number
// this will be considered its unique ID.
func CPUSerial() string {
	if HardwareID != "" {
		return HardwareID
	}

	// we have to use bash to be able to handle piping without using reader/writers
	output, err := exec.Command(`bash`, "-c", `cat /proc/cpuinfo | grep Serial`).Output()
	if err != nil {
		return ""
	}

	// rawSerial should be something like Serial     : serial number
	rawSerial := strings.Split(strings.Replace(string(output[:]), " ", "", -1), ":")

	if len(rawSerial) != 2 {
		return ""
	}

	// we only want to run this once per process, so set to a global variable and reference that from now on
	HardwareID = rawSerial[1]

	return HardwareID
}
