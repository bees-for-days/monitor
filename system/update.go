package system

import (
	"errors"
	"io"
	"io/ioutil"
	"log"
	"os"
	"time"
)

// UpdateBinary will accept an io.Reader which should hopefully be providing a binary program uploaded by the user.
// Then this function will remove the currently running binary, sleep to give time to the callee to warn of system
// shutdown, then shutdown the binary. We rely on supervisord, setup previously, to run the new binary
func UpdateBinary(r io.Reader) error {
	if r == nil {
		return errors.New("no file provided")
	}

	// first, remove the current binary, we don't check for errors because if it already doesn't exist we don't care
	os.Remove("./monitor")

	// typically, I wouldn't read an entire file into memory - but we're expecting the monitor binary to be very small
	// and never expect it to be larger than the 4gb of RAM the raspberry pi has. This might need refactored later, but
	// for now we're fine
	file, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile("./monitor", file, 0755)
	if err != nil {
		return err
	}

	go func() {
		log.Println("Binary updated, shutting down in 30 seconds...")
		time.Sleep(time.Second * 30)
		os.Exit(0) // we're exiting on purpose - normally supervisord wouldn't restart an expected exit but ours will
	}()

	return nil
}
