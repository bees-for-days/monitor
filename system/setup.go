package system

import (
	"log"
	"os/exec"
	"time"

	"gocv.io/x/gocv"
)

var webCam *gocv.VideoCapture
var InSetupMode = false
var stopCamera chan bool
var cameraUpdateSpeed chan bool

// HasAPClients runs a system command to see if there are any clients connected
// to the AP - used by wifi enabling loop for setup mode
func HasAPClients() bool {
	output, err := exec.Command("iw", "dev", "wlan0", "station", "dump").Output()
	if err != nil {
		return false
	}

	// if there is anything in the response it means we have someone connected
	// to the AP - all we care about is existence of even just single client
	return len(output) > 0
}

func StartSetupMode() error {
	InSetupMode = true
	return nil
}

func StopSetupMode() error {
	InSetupMode = false
	return StopCameraStream()
}

// StartCameraStream returns the streaming channel for the camera
func StartCameraStream() (chan []byte, error) {
	cameraUpdateSpeed := make(chan bool)
	stopCamera := make(chan bool)
	cameraStream := make(chan []byte)

	if webCam == nil || !webCam.IsOpened() {
		webcam, err := gocv.OpenVideoCapture("0")
		if err != nil {
			log.Println("Error opening capture device")
			return cameraStream, err
		}

		log.Println("Opened Device successfully")

		webCam = webcam
	}

	// first start the go routine which will allow the camera to run while
	// still listening to the stop camera channel
	go func() {
		for {
			time.Sleep(time.Millisecond * 10)
			cameraUpdateSpeed <- true

			if !InSetupMode {
				break
			}
		}
	}()

	// start the capture stream
	go func() {
		for {
			select {
			case <-stopCamera:
				break
			case <-cameraUpdateSpeed:
				img := gocv.NewMat()

				if ok := webCam.Read(&img); !ok {
					break
				}
				if img.Empty() {
					continue
				}

				buf, _ := gocv.IMEncodeWithParams(".jpg", img, []int{gocv.IMWriteJpegQuality, 100})
				cameraStream <- buf.GetBytes()

				img.Close()
			}
		}

	}()

	return cameraStream, nil
}

func StopCameraStream() error {
	log.Println("Stopping camera stream...")
	stopCamera <- true
	return webCam.Close()
}
