package main

import (
	"net/http"

	"gitlab.com/bees-for-days/monitor/system"

	"github.com/go-chi/render"
)

func updatedBinary(w http.ResponseWriter, r *http.Request) {
	// if our binary is greater than 1024mb then have a problem and shouldn't let this go on anyway
	if err := r.ParseMultipartForm(100 << 20); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	// we're very specific about what the file's name must be for the upload to work, little security
	// by obscurity
	file, _, err := r.FormFile("update")
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	defer file.Close()

	err = system.UpdateBinary(file)
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	render.Status(r, http.StatusOK)
}
